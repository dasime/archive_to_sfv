#!/usr/bin/env python3
import click

from archive_to_sfv.main import process_file


@click.command()
@click.argument("filename", type=click.Path(exists=True))
@click.option(
    "--truncate-nested-directory",
    "-t",
    is_flag=True,
    default=False,
    help="If all files are contained entirely within a common directory path, remove those directories from the output.",
)
def main(filename: str, truncate_nested_directory: bool) -> None:
    """
    Extract CRC32 metadata from an existing archive and save it to SFV.

    FILENAME is the archive to extract metadata from.
    """
    process_file(filename, truncate_nested_directory)


if __name__ == "__main__":
    main()

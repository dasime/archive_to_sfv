import sys

import click

from archive_to_sfv.archive import detect_archive_type

# Ensure the terminal is Unicode aware
if "pytest" not in sys.modules:  # pragma: no cover
    # the if is a workaround for pytest:
    #   https://github.com/pytest-dev/pytest/issues/4843
    # the type ignore is a workaround for mypy:
    #   https://github.com/python/typeshed/issues/3049
    sys.stdout.reconfigure(encoding="utf-8")  # type: ignore
    sys.stderr.reconfigure(encoding="utf-8")  # type: ignore


def process_file(archive_path: str, truncate_nested_directory: bool) -> None:
    try:
        process_archive = detect_archive_type(archive_path)
    except TypeError:
        click.echo("Unsupported archive format")
        return

    parent_dir, archive_files = process_archive(archive_path)

    if not len(archive_files):
        click.echo("Empty archive")
        return

    truncate_prefix = 0

    if truncate_nested_directory:
        truncate_prefix = len(parent_dir)

    for file in archive_files:
        click.echo(f"{file[0][truncate_prefix:]} {file[1]}")

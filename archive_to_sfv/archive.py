from typing import Any, Callable, List, Tuple
import zipfile

import rarfile  # type: ignore

from archive_to_sfv.helper import detect_parent, pretty_print_crc32


PROCESS_RETURN_VALUE = Tuple[str, List[Tuple[str, str]]]


def _process_zipfile_comptaible_archive(
    zipfile_like: Any,  # Any class that implements the ZipFile interface.
) -> PROCESS_RETURN_VALUE:
    parent_dir = detect_parent(zipfile_like.namelist())

    files_in_archive = [
        (file.filename, pretty_print_crc32(file.CRC))
        for file in zipfile_like.infolist()
        if not file.is_dir()
    ]

    return parent_dir, files_in_archive


def _process_rar(archive_path: str) -> PROCESS_RETURN_VALUE:
    with rarfile.RarFile(archive_path) as rf:
        return _process_zipfile_comptaible_archive(rf)


def _process_zip(archive_path: str) -> PROCESS_RETURN_VALUE:
    with zipfile.ZipFile(archive_path) as zf:
        return _process_zipfile_comptaible_archive(zf)


def detect_archive_type(
    archive_path: str,
) -> Callable[[str], PROCESS_RETURN_VALUE]:
    if archive_path.find(".rar") > 0:
        return _process_rar

    if archive_path.find(".zip") > 0:
        return _process_zip

    raise TypeError

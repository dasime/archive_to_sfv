import re
from typing import List


def detect_parent(member_list: List[str]) -> str:
    """
    Detects a common parent directory by sorting the contents members of an
    archive, and comparing each character of the first and last members.

    Where they match, we can assume everybody matches.

    :param      member_list:  A list of members of the archive.
    :type       member_list:  list

    :returns:   The common parent directory.
    :rtype:     str
    """
    if len(member_list) == 1:
        return ""

    # Drop directories
    member_list_sans_dirs = [
        member for member in member_list if member[-1:] != "/"
    ]

    member_list_sans_dirs.sort()
    detected_matching_prefix = ""

    first_member = member_list_sans_dirs[0]
    last_member = member_list_sans_dirs[len(member_list_sans_dirs) - 1]
    for char in range(len(first_member)):
        if first_member[char] != last_member[char]:
            break
        detected_matching_prefix += first_member[char]

    # if "/" not in detected_matching_prefix:
    #     return ""

    match = re.match(r"(.*/)", detected_matching_prefix)

    if match is not None:
        return match.group(0)
    else:
        return ""


def pretty_print_crc32(crc_value: int) -> str:
    return hex(crc_value)[2:].upper().zfill(8)

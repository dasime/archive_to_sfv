from archive_to_sfv.helper import detect_parent, pretty_print_crc32

from typing import List
import pytest


@pytest.mark.parametrize(
    "member_list, parent",
    [
        (
            [
                "bar",
            ],
            "",
        ),
        (
            [
                "foo/",
                "foo/bar",
                "foo/bam",
                "foo/baz",
                "bar/",
                "bar/bam",
            ],
            "",
        ),
        (
            [
                "foobar",
                "foo/",
                "foo/bam",
                "foo/baz",
            ],
            "",
        ),
        (
            [
                "foo/",
                "foo/bar",
                "foo/bam",
                "foo/baz",
            ],
            "foo/",
        ),
        (
            [
                "foo/",
                "foo/bar/",
                "foo/bar/bam",
                "foo/bar/baz",
            ],
            "foo/bar/",
        ),
        (
            [
                "foo/",
                "foo/bar/",
                "foo/bar/bam/",
                "foo/bar/bam/bam",
                "foo/bar/bam/baz",
            ],
            "foo/bar/bam/",
        ),
        (  # CJK
            [
                "汉语/",
                "汉语/日本語/",
                "汉语/日本語/한국어/",
                "汉语/日本語/한국어/bam/",
                "汉语/日本語/한국어/bam/bam",
                "汉语/日本語/한국어/bam/baz",
            ],
            "汉语/日本語/한국어/bam/",
        ),
    ],
)
def test_detect_parent(member_list: List[str], parent: str) -> None:
    assert detect_parent(member_list) == parent


@pytest.mark.parametrize(
    "crc32_int, crc32_str",
    [
        (0, "00000000"),
        (14463103, "00DCB07F"),
    ],
)
def test_pretty_print_crc32(crc32_int: int, crc32_str: str) -> None:
    assert pretty_print_crc32(crc32_int) == crc32_str

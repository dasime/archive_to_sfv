import archive_to_sfv.archive as archive

from typing import (
    Any,
    Callable,
    List,
    Tuple,
    Iterator,
    Optional,
    Type,
)
import pytest
from unittest.mock import patch, Mock


@pytest.mark.parametrize(
    "archive_path, archive_process",
    [
        ("./foobar.zip", archive._process_zip),
        ("./notrar.zip", archive._process_zip),
        ("./foobar.rar", archive._process_rar),
        ("./notzip.rar", archive._process_rar),
    ],
)
def test_detect_archive_type(
    archive_path: str, archive_process: Callable[[str], Any]
) -> None:
    assert archive.detect_archive_type(archive_path) == archive_process


@pytest.mark.parametrize(
    "archive_path",
    [
        ("./foobar.tar.bz2"),
        ("./foobar.tar.gz"),
        ("./foobar.xz"),
        ("./foobar.7z"),
        ("./foobar.gz"),
    ],
)
def test_detect_archive_type_unsupported(archive_path: str) -> None:
    with pytest.raises(TypeError):
        archive.detect_archive_type(archive_path)


class MockZipInfo:
    def __init__(
        self, filename: str, crc: int = 0x0, is_dir: bool = False
    ) -> None:
        self.filename = filename
        self._is_dir = True if filename[-1:] == "/" else False
        self.CRC = 0x0 if self._is_dir else crc

    def is_dir(self) -> bool:
        return self._is_dir


class MockZipFile:
    def __init__(self, files: List[MockZipInfo]) -> None:
        self._files = files

    def __iter__(self) -> Iterator:  # pragma: no cover
        return iter(self._files)

    def __enter__(self) -> "MockZipFile":
        return self

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_val: Optional[BaseException],
        exc_tb: Optional[Any],
    ) -> Optional[bool]:
        return True

    def infolist(self) -> List[MockZipInfo]:
        return self._files

    def namelist(self) -> List[str]:
        return [file.filename for file in self._files]


@pytest.mark.parametrize(
    "archive_contents, archive_item_list, files_in_archive",
    [
        (
            [
                MockZipInfo("foo/empty_dir/"),
                MockZipInfo("foo/bar"),
                MockZipInfo("foo/bam"),
                MockZipInfo("foo/"),
            ],
            ["foo/empty_dir/", "foo/bar", "foo/bam", "foo/"],
            [
                ("foo/bar", "00000000"),
                ("foo/bam", "00000000"),
            ],
        )
    ],
)
@patch("archive_to_sfv.archive.detect_parent")
def test__process_zipfile_comptaible_archive(
    detect_parent_mock: Mock,
    archive_contents: List[MockZipInfo],
    archive_item_list: List[str],
    files_in_archive: List[Tuple[str, str]],
) -> None:
    parent_dir = "foo"
    zipfile_like = MockZipFile(archive_contents)

    detect_parent_mock.return_value = parent_dir

    res = archive._process_zipfile_comptaible_archive(zipfile_like)

    detect_parent_mock.assert_called_with(archive_item_list)

    assert res == (parent_dir, files_in_archive)


@patch("archive_to_sfv.archive._process_zipfile_comptaible_archive")
@patch("archive_to_sfv.archive.rarfile.RarFile")
def test__process_rar(
    rarfile_mock: Mock,
    _process_zipfile_comptaible_archive_mock: Mock,
) -> None:
    archive_path = "foobar.ext"
    rf = MockZipFile([MockZipInfo("foo")])
    contents = ("", ["foo"])

    rarfile_mock.return_value = rf
    _process_zipfile_comptaible_archive_mock.return_value = contents

    res = archive._process_rar(archive_path)

    rarfile_mock.assert_called_with(archive_path)
    _process_zipfile_comptaible_archive_mock.assert_called_with(rf)

    assert res == contents


@patch("archive_to_sfv.archive._process_zipfile_comptaible_archive")
@patch("archive_to_sfv.archive.zipfile.ZipFile")
def test__process_zip(
    zipfile_mock: Mock,
    _process_zipfile_comptaible_archive_mock: Mock,
) -> None:
    archive_path = "foobar.ext"
    zf = MockZipFile([MockZipInfo("foo")])
    contents = ("", ["foo"])

    zipfile_mock.return_value = zf
    _process_zipfile_comptaible_archive_mock.return_value = contents

    res = archive._process_zip(archive_path)

    zipfile_mock.assert_called_with(archive_path)
    _process_zipfile_comptaible_archive_mock.assert_called_with(zf)

    assert res == contents

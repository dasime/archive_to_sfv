import archive_to_sfv.main as main

from typing import List, Tuple
import pytest
from unittest.mock import call, patch, Mock


file_list_1 = [("foo", "000000")]
file_list_2 = [
    ("foo/bar", "000000"),
    ("foo/bar/bam", "000000"),
]


@pytest.mark.parametrize(
    "parent_dir, file_list, truncate, response",
    [
        (
            "",
            file_list_1,
            False,
            [call(f"{file_list_1[0][0]} {file_list_1[0][1]}")],
        ),
        (
            "",
            file_list_1,
            True,
            [call(f"{file_list_1[0][0]} {file_list_1[0][1]}")],
        ),
        (
            "foo/",
            file_list_2,
            False,
            [
                call(f"{file_list_2[0][0]} {file_list_2[0][1]}"),
                call(f"{file_list_2[1][0]} {file_list_2[1][1]}"),
            ],
        ),
        (
            "foo/",
            file_list_2,
            True,
            [
                call(f"{file_list_2[0][0][4:]} {file_list_2[0][1]}"),
                call(f"{file_list_2[1][0][4:]} {file_list_2[1][1]}"),
            ],
        ),
    ],
)
@patch("archive_to_sfv.main.click.echo")
@patch("archive_to_sfv.main.detect_archive_type")
def test_process_file(
    archive_mock: Mock,
    click_mock: Mock,
    parent_dir: str,
    file_list: List[Tuple[str, str]],
    truncate: bool,
    response: List,
) -> None:
    archive_mock.return_value = lambda _: (parent_dir, file_list)

    main.process_file("foo.zip", truncate)

    archive_mock.assert_called_with("foo.zip")

    click_mock.assert_has_calls(response)


@patch("archive_to_sfv.main.click.echo")
@patch("archive_to_sfv.main.detect_archive_type")
def test_process_file_unsupported_type(
    archive_mock: Mock,
    click_mock: Mock,
) -> None:
    archive_mock.side_effect = TypeError()

    main.process_file("foo.zip", False)

    archive_mock.assert_called_with("foo.zip")

    click_mock.assert_called_with("Unsupported archive format")


@patch("archive_to_sfv.main.click.echo")
@patch("archive_to_sfv.main.detect_archive_type")
def test_process_file_empty(
    archive_mock: Mock,
    click_mock: Mock,
) -> None:
    archive_mock.return_value = lambda _: ("parent_dir", [])

    main.process_file("foo.zip", False)

    archive_mock.assert_called_with("foo.zip")

    click_mock.assert_called_with("Empty archive")

# Archive to SFV

Extract CRC32 values from the metadata in an archive, and store them separately in a SFV.

## SFV Format

SFV format notes here: <https://en.wikipedia.org/wiki/Simple_file_verification>

Highlights:

- Any line starting with a semicolon ';' is considered to be a comment and is ignored
  for the purposes of file verification.
- Each file is on it's own line, in the following `FILENAME<whitespace>CHECKSUM` format.

Example:

```
file_one.zip   c45ad668
file_two.zip   7903b8e6
file_three.zip e99a65fb
```

## Usage

```
$ archive_to_sfv --help
Usage: archive_to_sfv [OPTIONS] FILENAME

  Extract CRC32 metadata from an existing archive and save it to SFV.

  FILENAME is the archive to extract metadata from.

Options:
  -t, --truncate-nested-directory
                                  If all files are contained entirely within a
                                  common directory path, remove those
                                  directories from the output.

  --help                          Show this message and exit.
```

for example:

```
$ archive_to_sfv -t my_archive.rar > verify.sfv
```

Redirecting a stream output containing Unicode characters as above is supported even on Windows.

## Important Note About RAR support.

The library used to work with RAR files - [rarfile](https://rarfile.readthedocs.io/) - requires
one of [unrar](https://www.rarlab.com/), [unar](https://theunarchiver.com/command-line) or
[bsdtar](https://github.com/libarchive/libarchive) to be installed and on to be accessible
on the path to open compressed RAR files.

There are instructions for this available on the
[rarfile support pages](https://rarfile.readthedocs.io/faq.html).

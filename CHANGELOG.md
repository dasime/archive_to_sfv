# Changelog

## 2.0.0 / 2020-10-03

* Added new Python version of the project.
* Removed the 2016 Node version of the project.
* Added proper CLI application installation support.  
  __No more aliasing faff__
* Added support for reading archives directly.  
  __No more 7-zip middleman†__
* Added support for piping CJK output on Windows in `cmd.exe`.  
  __MinGW now optional__
* Added support for truncating empty directories in output.  
  __No more editing sfv files manually__
* Added comprehensive testing.

† `rarfile` support is provided by third-party libraries. See README.md.

## 1.0.0 / 2016-04-10

* Initial version

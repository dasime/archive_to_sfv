# Contributing

## To Build

```
poetry build
```

tarball and wheel are built to `./dist`

## To Test

```
poetry run task lint
poetry run task test
```

Changes must pass both tasks.
